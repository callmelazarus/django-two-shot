from venv import create
from django.urls import path
from receipts.views import (
    show_categories,
    show_receipts,
    create_receipt,
    show_accounts,
    create_expensecategory,
    create_account
)

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_categories, name="show_categories"),
    path("accounts/", show_accounts, name="show_accounts"),
    path("categories/create/", create_expensecategory, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),

]
