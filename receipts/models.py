from django.db import models
from django.contrib.auth.models import User
from django.forms import CharField  # added user model that django has set up
from django.utils import timezone

# Create your models here.


class ExpenseCategory(models.Model):  # various expenses like giving, entertainment
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(User, related_name="categories", on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Account(models.Model):  # way we pay for something
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(User, related_name="accounts", on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Receipt(models.Model):  # receipts that track transactions
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField(auto_now_add=False, default=timezone.now)  # created when transaction takes place
    purchaser = models.ForeignKey(User, related_name="receipts", on_delete=models.CASCADE, null=True)  # MANY receipts for a purchaser/user
    category = models.ForeignKey("ExpenseCategory", related_name="receipts", on_delete=models.CASCADE)  # Many receipts for an exapense category
    account = models.ForeignKey("Account", related_name="receipts", on_delete=models.CASCADE, null=True)
