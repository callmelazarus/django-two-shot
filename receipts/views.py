from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm
from receipts.models import ExpenseCategory, Account, Receipt

@login_required
def show_receipts(request):
    # receipts = Receipt.objects.all() # no longer getting the receipts for everyone
    receipts_for_user = (request.user.receipts.all())  # adjusted to get receipts for one user

    # alternate way to get the receipts for a user, using filter method
    # receipts_for_user_alt = Receipt.objects.filter(purchaser=request.user)
    
    context = {"receipts": receipts_for_user}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        # need to add code so that the logged in USER is associated with the receipt
        username = request.user # saves signed in user to username
        if form.is_valid():
            # need to somehow assign the username to the Receipt.purchase attribute...
            # Receipt.purchaser = username does not work...
            receipt = form.save(commit=False) # by setting commit = False -> this will return the form as an object 'receipt' that you can change...
            receipt.purchaser = username
            receipt.save() # save this instance of the 'form'
        return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)


@login_required
def show_categories(request):
    expense_categories = ExpenseCategory.objects.filter(owner=request.user) # getting all expense category info for a user
    context = {
        "expense_categories": expense_categories
    }
    return render(request, "receipts/showcategories.html", context)


@login_required
def show_accounts(request):
    #there are many receipts to one account
    account_info = Account.objects.filter(owner=request.user) # getting all the account info for a user
    context = {
        "account_info": account_info
    }
    return render(request, "receipts/showaccounts.html", context)



@login_required
def create_expensecategory(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        # need to add code so that the logged in USER is associated with the expenseform
        username = request.user # saves signed in user to username
        if form.is_valid():
            # need to somehow assign the username to the Receipt.purchase attribute...
            # Receipt.purchaser = username does not work...
            expensecat = form.save(commit=False) # by setting commit = False -> this will return the form as an object 'receipt' that you can change...
            expensecat.owner = username
            expensecat.save() # save this instance of the 'form'
        return redirect('show_categories')
    else:
        form = ExpenseForm()
    context = {
        "form": form
    }
    return render(request, "receipts/createcategory.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        # need to add code so that the logged in USER is associated with the expenseform
        username = request.user # saves signed in user to username
        if form.is_valid():
            # need to somehow assign the username to the Receipt.purchase attribute...
            # Receipt.purchaser = username does not work...
            expensecat = form.save(commit=False) # by setting commit = False -> this will return the form as an object 'expensecat' that you can change...
            expensecat.owner = username
            expensecat.save() # save this instance of the 'form'
        return redirect('show_accounts')
    else:
        form = AccountForm()
    context = {
        "form": form # this actually wants to be form, not expensecat
    }
    return render(request, "receipts/createaccount.html", context)
