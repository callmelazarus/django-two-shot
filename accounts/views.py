from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User


# view that handles a sign-up form -> and logs that account in after POST
# iteration 1 - does not use create_user..... isn't that something UserCreationForm does??
def signup_view(request):
    if request.method == "POST":  # if the request is a POST
        form = UserCreationForm(request.POST)  # saves UserCreationForm, filled out into 'form'
        if form.is_valid():  # checks if form is valid
            user = form.save()  # will save form, and create a new user -> save to 'user'
            login(request, user)
            return redirect("home")  # bring them home if they signed up
    else:  # if the request is a not-POST/GET
        form = UserCreationForm()  # creates a blank instance of this form -> used to show BLANK form
    context = {"form": form}  # form has form data
    return render(request, "registration/signup.html", context)


# ## Matt's iteration:
# def signup(request):
#     if request.method == "POST":
#         form = UserCreationForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get("username")
#             raw_password = form.cleaned_data.get("password1")
#             user = authenticate(username=username, password=raw_password)
#             login(request, user)
#             return redirect("home")
#     else:
#         form = UserCreationForm()
#     return render(request, "registration/signup.html", {"form": form})
